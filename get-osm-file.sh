#!/bin/sh

region="saitama"
newfile="$region-`date '+%Y-%m-%d'`.osm"
oldfile="$region-`date --date '1 month ago' +%Y-%m-%d`.osm.bz2"
destdir="/home/kmr/smb/onedrive/osm-saitama/"
mountdir="/home/kmr/saitama-region/"
ftpdir="$mountdir/www/bz2"
cmd=`dirname "$0"`

cd $cmd

wget http://download.geofabrik.de/asia/japan-latest.osm.pbf

osmosis --read-pbf file="japan-latest.osm.pbf" --bounding-polygon file="$region.poly" --write-xml file="$region.osm"

rm japan-latest.osm.pbf

mv $region.osm $newfile

bzip2 $newfile

curlftpfs ftp://saitama-region.sakura.ne.jp $mountdir
cp $newfile.bz2 $ftpdir
rm $ftpdir/$oldfile
fusermount -u $mountdir

#scp ./$newfile.bz2 x40:/home/kmr/smb/OneDrive/osm-saitama/

#mv $newfile.bz2 $destdir
rm $newfile.bz2

exit 0
